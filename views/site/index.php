<?php

/** @var yii\web\View $this */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">CONSULTAS DE SELECCIÓN 2</h1>
    </div>

    <div class="body-content">
        <div class="card alturaminima">
            <div class="card-body tarjeta">
                <h3>Consulta 1</h3>
                <p></p>
                <p>
                    <?= Html::a('Active Record',['site/consulta1a'],['class'=>'btn btn-primary'])?>
                    <?= Html::a('Active Record',['site/consulta1'],['class'=>'btn btn-warning'])?>
                </p>
            </div>
        </div>
        

    </div>
</div>
